<?php 
get_header();

$article_is_hot = tu_get_article_with_pagination(1, 1, array('article_is_hot' => 'true'));
$terms_article = tu_get_terms_by_parent_id( 'article_category', 0);
$current_term_id = get_queried_object()->term_id;
$term_posts = tu_get_article_with_pagination(1, -1, array('article_category' => $current_term_id));
?> 
<div class="page-news">
	<div class="banner" style="background-image: url('<?php echo IMAGE_URL.'/news/banner.jpg'; ?>');"></div>
	<div class="hot-news">
		<div class="ti_tieude">
			<div class="tieude_label">
				<h1>tin nổi bật</h1>
			</div>
		</div>
		<?php if ( $article_is_hot->have_posts() ) : ?>
            <?php while ( $article_is_hot->have_posts() ) : $article_is_hot->the_post(); ?>
                <?php
                $article_id = get_the_ID();
                $article_thumbnail_url = get_the_post_thumbnail_url( $article_id, 'full' );
                $article_permalink = get_the_permalink($article_id);
                $article_title = get_the_title($article_id);
                $article_excerpt = get_the_excerpt($article_id);
                $article_date = get_the_date('d/m/Y');
                ?>
               
				<div class="content">
					<div class="left">
						<div class="tieude_label">
							<h2>
								<?php echo $article_title; ?>
							</h2>
							<!-- <h1>CROWN VILLAS</h1>
							<h2>nghệ thuật hưởng thụ cuộc sống</h2> -->
						</div>
						<div class="date">
							<i class="fa fa-clock-o" aria-hidden="true"></i><p><?php echo $article_date; ?></p>
						</div>
						<div class="text">
							<?php echo $article_excerpt; ?>
						</div>
						<a class="but_xemthem" href="<?php echo $article_permalink; ?>">xem thêm</a>
					</div>
					<div class="right">
						<img src="<?php echo $article_thumbnail_url; ?>" alt="">
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<?php 
	    if ($terms_article) :
	?>
	<div class="list-news" id="js-tab">
		<div class="ti_tieude">
			<div class="tieude_label">
				<h1>Tin khác</h1>
			</div>
		</div>
		<div class="content">
			<div class="nav">
				<?php foreach ( $terms_article as $term ) : ?>
					<?php
					$terms_article_id = $term->term_id;
					$term_slug = $term->slug;
					$terms_article_name = $term->name;
					$term_link = get_term_link($terms_article_id);
					$active = $current_term_id == $terms_article_id ? 'is-active' : '' ;
					?>
					<a href="<?php echo $term_link ?>" class="<?php echo $active; ?>"><?php echo $terms_article_name ?></a>
				<?php endforeach; ?>
			</div>
			<div class="desc">
				<?php
                    if ($term_posts->have_posts()) :
                        while ($term_posts->have_posts()) :
                            $term_posts->the_post();
                            $post_id = $post->ID;
	 						$article_thumbnail_url = get_the_post_thumbnail_url( $post_id, 'full' );
		                    $article_permalink = get_the_permalink($post_id);
		                    $article_title = get_the_title($post_id);
		                    $article_excerpt = wp_trim_words(get_the_excerpt($post_id), 23);
		                    $article_date = get_the_date('d/m/Y');
                ?>
					<a href="<?php echo $article_permalink; ?>" class="item">
						<div class="thumbnail">
							<div class="img" style="background-image: url('<?php echo $article_thumbnail_url; ?>');">
							</div>
						</div>
						<div class="text">
							<div class="the-title">
								<?php echo $article_title; ?>
							</div>
							<div class="date">
								<i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $article_date; ?>
							</div>
							<div class="desc">
								<?php echo $article_excerpt; ?>
							</div>
							<div class="more">
								xem thêm
							</div>
						</div>
					</a>
				<?php
                        endwhile;
                    endif;
                ?>
			</div>
		</div>
	</div>
	<?php
	    else : _e("Bài viết đang cập nhật", TEXT_DOMAIN);
	    endif; 
	?>
</div>
<script>
	jQuery(document).ready(function($){
		
	});
</script>
<?php get_footer(); ?>