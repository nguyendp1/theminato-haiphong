<?php $_siteInfo = array(
    'app_id' => FACEBOOK_APP_ID,// App Name: TU
    'type' => 'website',// Need to be changed with each type of pages
    'title' => get_bloginfo('name'),// Site Title
    'url' => home_url(),// Permalink
    'image' => get_template_directory_uri() . '/screenshot.jpg?v='.date('dmY'),// Screenshot, Thumbnail
    'description' => get_bloginfo('description'),// Tagline, Excerpt
    'author' => 'Time Universal',// Change manually
);

if( is_tax() || is_category() || is_tag() ) {
    if( !is_category() )
        $_siteInfo['url'] = get_term_link( get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    else
        $_siteInfo['url'] = get_category_link( get_query_var( 'cat' ) );

    $_siteInfo['title'] = single_term_title( '', false ) .' | '. get_bloginfo('name');
    $_siteInfo['description'] = term_description() ? term_description() : $_siteInfo['description'];
    $_siteInfo['type'] = 'article';
}

if( is_search() ) {
    $_siteInfo['title'] = 'Search: '. esc_html( get_query_var('s') ) .' | '. get_bloginfo('name');
    $_siteInfo['description'] = 'Search result for "'. esc_html( get_query_var('s') ) .'" from '. get_bloginfo('name');
}

if( is_author() ) {
    $authorID = get_query_var('author');
    $authorData = get_userdata( $authorID );
    $_siteInfo['title'] = $authorData->display_name .' @ '. get_bloginfo('name');
    $_siteInfo['description'] = $authorData->description ? $authorData->description : $_siteInfo['description'];
}

if( is_single() || is_page() ) {
    $imageSource = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
    $postDescription = $post->post_excerpt ? $post->post_excerpt : wp_trim_words( $post->post_content );
    $_siteInfo['title'] = $post->post_title;
    $_siteInfo['description'] = $postDescription ? strip_tags( $postDescription ) : $_siteInfo['description'];
    $_siteInfo['image'] = $imageSource ? $imageSource[0] : $_siteInfo['image'];
    $_siteInfo['url'] = get_permalink( $post->ID );
    $_siteInfo['type'] = 'article';
    // $_siteInfo['author'] = get_the_author_meta( 'display_name', $post->post_author );
}

if( is_paged() ) {
    $_siteInfo['title'] .= ' | '.__('Trang', TEXT_DOMAIN).' '. get_query_var('paged');
    $_siteInfo['description'] .= ' | '.__('Trang', TEXT_DOMAIN).' '. get_query_var('paged');
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="author" content="<?php echo str_replace('"', '', $_siteInfo['author']);?>" />
    <meta name="description" content="<?php echo str_replace('"', '', $_siteInfo['description']);?>" />
    <meta property="fb:app_id" content="<?php echo $_siteInfo['app_id'];?>" />
    <meta property="og:type" content='<?php echo $_siteInfo['type'];?>' />
    <meta property="og:title" content="<?php echo str_replace('"', '', $_siteInfo['title']);?>" />
    <meta property="og:url" content="<?php echo $_siteInfo['url'];?>" />
    <meta property="og:image" content="<?php echo $_siteInfo['image'];?>" />
    <meta property="og:description" content="<?php echo str_replace('"', '', $_siteInfo['description']);?>" />
    <link type="image/x-icon" rel="shortcut icon" href="<?php echo IMAGE_URL; ?>/favicon.png">

    <title><?php echo esc_html( $_siteInfo['title'] ); ?></title>

    <?php wp_head();?>
</head>
<body <?php body_class(); ?>>
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <?php if( is_home() ): ?>
    <?php endif; ?>  

    <div class="header">
        <a href="#" class="logo">
            <img src="<?php echo IMAGE_URL .'/home/header-footer-s9/logo-header.png'?>" alt="">
            <img src="<?php echo IMAGE_URL .'/home/header-footer-s9/logo-header1.png'?>" alt="">
        </a>
        <ul class="menu">
            <li class="item-menu"><a href="#">about us</a></li>
            <li class="item-menu"><a href="#">location</a></li>
            <li class="item-menu"><a href="#">utilities</a></li>
            <li class="item-menu"><a href="#">drawing</a></li>
            <li class="item-menu"><a href="#">model house</a></li>
            <li class="item-menu"><a href="#">new</a></li>
            <li class="item-menu"><a href="#">progress</a></li>
            <li class="item-menu"><a href="#">gallery</a></li>
            <li class="item-menu"><a href="#">contact</a></li>
            <li class="item-menu">
                <span>Vn</span>
                <span>En</span>
            </li>
        </ul>
    </div>
    <div class="logo-mobile">
            <a href="<?php echo HOME_URL; ?>"> <img src="<?php echo IMAGE_URL .'/home/header-footer-s9/logo-header.png'?>" alt=""></a>
    </div>
    <div class="header-menu-mobile">
    </div>
    <div class="humberger">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
    </div>
    <div class="menu-mobile">
        <a href="" class="logo-mobile-menu">
         <img src="<?php echo IMAGE_URL .'/home/header-footer-s9/logo-header.png'?>" alt="">
     </a>
     <ul class="main-menu-mobile">
        <li class="item-menu"><a href="#">about us</a></li>
        <li class="item-menu"><a href="#">location</a></li>
        <li class="item-menu"><a href="#">utilities</a></li>
        <li class="item-menu"><a href="#">drawing</a></li>
        <li class="item-menu"><a href="#">model house</a></li>
        <li class="item-menu"><a href="#">new</a></li>
        <li class="item-menu"><a href="#">progress</a></li>
        <li class="item-menu"><a href="#">gallery</a></li>
        <li class="item-menu"><a href="#">contact</a></li>
    </ul>
</div>
<script>
    jQuery(document).ready(function($) { 
             // js-header-menu
             $('.humberger').click(function() {
                $('.menu-mobile').toggleClass('menu-mobile-active');
                $('.humberger').toggleClass('humberger-active');

            });

             $(window).scroll(function () {
                var y = $(window).scrollTop(); 
                if (y > 20) { 
                    $('.header').addClass('scroll-header'); 
                }  
                else{
                   $('.header').removeClass('scroll-header'); 
               }    
           });
         });
     </script>