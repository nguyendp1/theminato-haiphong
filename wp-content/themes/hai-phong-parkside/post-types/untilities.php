<?php

/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_untilites');
function tu_reg_post_type_untilites() {
    //Change this when creating post type
    $post_type_name = __('Danh sách tiện ích', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', '', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('untilites', $args);

    /** Register Taxonomies */
    register_taxonomy('untilites_category', array('untilites'), array(
        "hierarchical" => true,
        "label" => __('Chuyên mục tiện ích', TEXT_DOMAIN),
        "singular_label" => __('Chuyên mục tiện ích', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('tien-ich', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    ));
    
}

function tu_get_untilites_with_pagination($page = 1, $post_per_page = 10, $custom_args = array()) {
    $args = array(
        'post_type' => 'untilites',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    // Push Taxonomy
    if (isset($custom_args['untilites_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'untilites_category',
            'field' => 'id',
            'terms' => $custom_args['untilites_category']
        ));

        unset($custom_args['untilites_category']);
    }

    // Push Taxonomy
    if (isset($custom_args['ld_subdivision_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'ld_subdivision_category',
            'field' => 'id',
            'terms' => $custom_args['ld_subdivision_category']
        ));

        unset($custom_args['ld_subdivision_category']);
    }

    $args = array_merge($args, $custom_args);
    
    $posts = new WP_Query($args);

    return $posts;
}

/**
 * Term metabox untilites_category in untilites
 */
add_action('untilites_category_edit_form_fields', 'display_metabox_untilites_category', 99, 2 );
function display_metabox_untilites_category( $term ){
    $term_id = $term->term_id;
    $untilites_category_des = get_term_meta($term_id, 'untilites_category_des', true);
    ?>
    <?php tu_render_image_array_by_term_id_and_name('Banner', $term_id, 'untilites_category_banner'); ?>
    <?php tu_render_image_array_by_term_id_and_name('Slide ảnh', $term_id, 'untilites_category_slide'); ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><label for="untilites_category_des">Mô tả</label></th>
                <td>
                    <?php wp_editor($untilites_category_des, 'untilites_category_des', array('wpautop' => true, 'textarea_name' => 'untilites_category_des', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}


/*
    Save metabox taxonomy untilites_category
*/
    add_action('create_untilites_category', 'save_metabox_untilites_category', 10, 2 );
    add_action('edited_untilites_category', 'save_metabox_untilites_category', 10, 2 );
    function save_metabox_untilites_category( $term_id ){
        if (isset($_POST['untilites_category_banner'])) {
            update_term_meta($term_id, 'untilites_category_banner', $_POST['untilites_category_banner']);
        }
        if (isset($_POST['untilites_category_slide'])) {
            update_term_meta($term_id, 'untilites_category_slide', $_POST['untilites_category_slide']);
        }
        if (isset($_POST['untilites_category_des'])) {
            update_term_meta($term_id, 'untilites_category_des', $_POST['untilites_category_des']);
        }

    }




