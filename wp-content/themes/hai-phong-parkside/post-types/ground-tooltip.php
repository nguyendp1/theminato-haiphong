<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_ground_tooltip');
function tu_reg_post_type_ground_tooltip() {
    //Change this when creating post type
    $post_type_name = __('Danh sách tooltip căn hộ', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới ', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem ', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('ground_tooltip', $args); 

}
/**
* Get ground-tooltip
*
* @param int   $page
* @param int   $post_per_page
* @param array $custom_args
*
* @return WP_Query
*/
function tu_get_ground_tooltip_with_pagination($page = 1, $post_per_page = 10, $options = array()) {

    $args = array(
        'post_type' => 'ground_tooltip',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    $posts = new WP_Query($args);
    
    return $posts;
}

/**
 * POST META BOXES ----------- ----------- -----------
 */


add_action('admin_init', 'tu_add_meta_box_ground_tooltip');
function tu_add_meta_box_ground_tooltip() {

    function tu_display_meta_box_ground_tooltip($post) {
        $post_id = $post->ID;
        $g_tooltip_code = get_post_meta($post_id, 'g_tooltip_code', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_ground_tooltip'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="g_tooltip_code">Mã tooltip</label></th>
                    <td>
                        <textarea name="g_tooltip_code" id="g_tooltip_code" cols="53" rows="1"><?php echo $g_tooltip_code;  ?></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_ground_tooltip', 'Thông tin', 'tu_display_meta_box_ground_tooltip', 'ground_tooltip', 'normal', 'high');

}

add_action('save_post', 'tu_save_meta_ground_tooltip');
function tu_save_meta_ground_tooltip($post_id) {
    if ( isset( $_POST['g_tooltip_code'] ) ) {
        update_post_meta($post_id, 'g_tooltip_code', $_POST['g_tooltip_code']);
    }
}