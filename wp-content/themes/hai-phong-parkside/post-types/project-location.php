<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_repost_type_project_location');
function tu_repost_type_project_location() {
    //Change this when creating post type
    $post_type_name = __('Vị trí dự án', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới ', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem ', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('project_location', $args); 

}

function tu_get_project_location_with_pagination($page = 1, $post_per_page = 10, $options = array()) {

    $args = array(
        'post_type' => 'project_location',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    $posts = new WP_Query($args);
    
    return $posts;
}

/**
 * POST META BOXES ----------- ----------- -----------
 */

add_action('admin_init', 'tu_add_post_meta_project_location');
function tu_add_post_meta_project_location() {
    /** Meta box for general information */
    function tu_display_post_meta_project_location($post) {
        $post_id = $post->ID;

        echo '<input type="hidden" name="nonce" value="' . wp_create_nonce('save_meta_box_project_location') . '">';

        $title = get_post_meta($post_id, 'title', true );
        $description = get_post_meta($post_id, 'description', true);
        $image = get_post_meta($post_id, 'image', true);  

        $metaboxData = array (

            array (
                'label' => __('Title', TEXT_DOMAIN),
                'name' => 'title',
                'value' => $title,
                'type' => 'input',
            ),
            
            array (
                'label' => __('Mô tả ngắn', TEXT_DOMAIN),
                'name' => 'description',
                'value' => $description,
                'type' => 'editor',
            ),
            array (
                'label' => __('Địa điểm', TEXT_DOMAIN),
                'name' => 'image',
                'value' => $image,
                'type'  => 'upload-image-album'
            ), 

        );

        metaboxPrint($metaboxData);

    }

    add_meta_box (
        'tu_display_post_meta_project_location', __('Thông tin vị trí dự án', TEXT_DOMAIN), 'tu_display_post_meta_project_location', 'project_location', 'normal', 'high'
    );
}

add_action('save_post', 'tu_save_post_meta_project_location');
function tu_save_post_meta_project_location($post_id) {
    if (get_post_type() == 'project_location' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_meta_box_project_location')) {

        if ( isset($_POST['title']) ) {
            update_post_meta($post_id, 'title', sanitize_text_field($_POST['title']));
        }

        if ( isset($_POST['description']) ) {
            update_post_meta($post_id, 'description', ($_POST['description']));
        } 

        if ( isset($_POST['image'] ) ) {
            update_post_meta($post_id, 'image', $_POST['image']);
        }else {
            delete_post_meta($post_id, 'image');
        }

    }
}