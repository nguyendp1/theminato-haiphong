<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_article');
function tu_reg_post_type_article() {
    //Change this when creating post type
    $post_type_name = __('Tin tức', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('article', $args);
    
    /** Register Taxonomies */

    register_taxonomy('article_category', array('article'), array(
        "hierarchical" => true,
        "label" => __('Chuyên mục tin tức', TEXT_DOMAIN),
        "singular_label" => __('Chuyên mục tin tức', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('tin-tuc', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    ));
}

/**
 * RETRIEVING FUNCTIONS ----------- ----------- -----------
 */

/**
 * Get articles
 *
 * @param int   $page
 * @param int   $post_per_page
 * @param array $custom_args
 *
 * @return WP_Query
 */
function tu_get_article_with_pagination($page = 1, $post_per_page = 10, $custom_args = array()) {

    $args = array(
        'post_type' => 'article',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array(),
        'meta_query' => array()
    );

    if (isset($custom_args['article_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'article_category',
            'field' => 'id',
            'terms' => $custom_args['article_category']
        ));

        unset($custom_args['article_category']);
    }

     // Push article is hot
    if (isset($custom_args['article_is_hot'])) {

        array_push($args['meta_query'], array(
            'key'     => 'article_is_hot',
            'value'   => $custom_args['article_is_hot'],
            'compare' => '=',
        ));

        unset($custom_args['article_is_hot']);
    }

    $args = array_merge($args, $custom_args);

    $posts = new WP_Query($args);

    return $posts;
}

/**
 * Get html of each article in loop
 * This is used in while loop after the_post()
 *
 * @param string $custom_class
 *
 * @return string
 */
function tu_get_each_article_html($custom_class = "") {

    $post_id = get_the_ID();
    $title = get_the_title();

    $permalink = get_permalink($post_id);

    $thumbnail_html = has_post_thumbnail() ? get_the_post_thumbnail($post_id, 'thumbnail') : '<img src="'.NO_IMAGE_URL.'" />';
    $categories_html = tu_get_post_terms($post_id, 'article_category', true, 'itemprop="genre" class="item-category"');
    $post_date = get_the_date('d/m/Y');
    $excerpt = wp_trim_words(get_the_excerpt());

    $html = '
    <div class="col-md-12 '.$custom_class.'">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">'.$thumbnail_html.'</div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <h3 class="item-title"><a href="'.$permalink.'">'.$title.'</a></h3>
                <p class="item-categories">'.$categories_html.'</p>
                <p class="item-date">'.$post_date.'</p>
                <p class="item-description">'.$excerpt.'</p>
            </div>
        </div>
    </div>
    ';

    return $html;
}

/**
 * Display each article in loop
 * This is used in while loop after the_post()
 *
 * @param string $custom_class
 *
 * @return string
 */
function tu_display_each_article($custom_class = ""){
    echo tu_get_each_article_html($custom_class);
}

/**
 * POST META BOXES ----------- ----------- -----------
 */

add_action('admin_init', 'tu_add_meta_box_article');
function tu_add_meta_box_article() {

    /** Meta box for general information */
    function tu_display_meta_box_article_general($post) {
        $post_id = $post->ID;
        $article_is_hot = get_post_meta($post_id, 'article_is_hot', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_article'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="article_is_hot"><?php _e('Bài viết nổi bật', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="hidden" name="do" value="post"/>
                        <input type="checkbox" id="article_is_hot" name="article_is_hot"
                               value="1" <?php if ($article_is_hot == 'true') echo 'checked="checked"'; ?> />
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box(
            'tu_display_meta_box_article_general', 'Thông tin cơ bản', 'tu_display_meta_box_article_general', 'article', 'normal', 'high'
    );
}

add_action('save_post', 'tu_save_meta_box_article');
function tu_save_meta_box_article($post_id) {
    if (get_post_type() == 'article' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_meta_box_article')) {

        $article_is_hot = isset($_POST['article_is_hot']) && (int) $_POST['article_is_hot'] ? 'true' : 'false';
        update_post_meta($post_id, 'article_is_hot', $article_is_hot);
    }
}

/**
 * TERMS META BOXES ----------- ----------- -----------
 */

add_action('article_category_edit_form_fields', 'tu_display_meta_box_article_category', 99, 2);
function tu_display_meta_box_article_category($term) {
    if ($term->parent == 0):
        $article_category_image_id = get_term_meta($term->term_id, 'article_category_image_id', true);
        $article_category_display_title = get_term_meta($term->term_id, 'article_category_display_title', true);
        $article_category_color = get_term_meta($term->term_id, 'article_category_color_hex_code', true);
        ?>
        <table id="ads" class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="article_category_display_title"><?php _e('Tên hiển thị', TEXT_DOMAIN) ?></label></th>
                    <td>
                        <input type="text" id="article_category_display_title" class="regular-text" name="article_category_display_title" value="<?php echo $article_category_display_title; ?>"/>
                    </td>
                </tr>
                <tr class="upload-row">
                    <th scope="row">
                        <label for=""><?php _e('Icon', TEXT_DOMAIN) ?></label>
                    </th>
                    <td>
                        <button type="button" class="button upload-button">Chọn ảnh (50x50px)</button>
                        <input type="hidden" class="upload-files-ids" name="article_category_image_id" value="<?php echo $article_category_image_id; ?>">
                        <div class="upload-files-preview"><?php echo tu_get_images_html_by_attachment_ids($article_category_image_id); ?></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="article_category_color_hex_code">
                            <?php _e('Mã màu', TEXT_DOMAIN) ?>
                            <br/><span style="display: inline-block; margin-top: 10px; width: 50px; height: 50px; background: <?php echo $article_category_color ? : '#FFF'; ?>"></span>
                        </label></th>
                    <td>
                        <input type="text" id="article_category_color_hex_code" class="regular-text" name="article_category_color_hex_code" value="<?php echo $article_category_color; ?>"/>
                        <p class="description">VD: #FFFFFF</p>
                    </td>
                </tr>
            </tbody>
        </table>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                //Single Image Upload
                jQuery('.upload-row').delegate('.upload-button', 'click', function (event) {
                    var _this = jQuery(this);

                    var file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select image',
                        library: {},
                        button: {text: 'Select'},
                        //frame: 'post',
                        multiple: false
                    });

                    file_frame.on('select', function () {
                        var attachment = file_frame.state().get('selection').first().toJSON();
                        var _this_parent = _this.parents('.upload-row');
                        _this_parent.find('.upload-files-ids').val(attachment.id);
                        _this_parent.find('.upload-files-preview').html('<img width="565px" src="' + attachment.url + '" />');
                    });

                    file_frame.open();
                });
            });
        </script>

        <style type="text/css">
            .upload-files-preview img {
                max-width: 100%;
                margin-right: 1%;
            }
        </style>
    <?php
    endif;
}

add_action('create_article_category', 'tu_save_meta_box_article_category', 10, 2);
add_action('edited_article_category', 'tu_save_meta_box_article_category', 10, 2);
function tu_save_meta_box_article_category($term_id) {

    if (isset($_POST['article_category_image_id']) && $_POST['article_category_image_id']) {
        update_term_meta($term_id, 'article_category_image_id', $_POST['article_category_image_id']);
    }else{
        delete_term_meta($term_id, 'article_category_image_id');
    }

    if (isset($_POST['article_category_display_title']) && $_POST['article_category_display_title']) {
        update_term_meta($term_id, 'article_category_display_title', $_POST['article_category_display_title']);
    }else{
        delete_term_meta($term_id, 'article_category_display_title');
    }

    if (isset($_POST['article_category_color_hex_code']) && $_POST['article_category_color_hex_code']) {
        update_term_meta($term_id, 'article_category_color_hex_code', $_POST['article_category_color_hex_code']);
    }else{
        delete_term_meta($term_id, 'article_category_color_hex_code');
    }

}
