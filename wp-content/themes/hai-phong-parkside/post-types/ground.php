<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_ground');
function tu_reg_post_type_ground() {
    //Change this when creating post type
    $post_type_name = __('Căn hộ', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới ', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa ', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem ', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('ground', $args); 

    /** Register Taxonomies */

    register_taxonomy('ground_subdivision', array('ground'), array(
        "hierarchical" => true,
        "label" => __('Phân khu căn hộ', TEXT_DOMAIN),
        "singular_label" => __('Phân khu căn hộ', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('phan-khu-can-ho', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    )); 

}
/**
* Get ground
*
* @param int   $page
* @param int   $post_per_page
* @param array $custom_args
*
* @return WP_Query
*/
function tu_get_ground_with_pagination($page = 1, $post_per_page = 10, $options = array()) {

    $args = array(
        'post_type' => 'ground',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    if (isset($options['ground_subdivision'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'ground_subdivision',
            'field' => 'term_id',
            'terms' => $options['ground_subdivision']
        ));
    }

    $posts = new WP_Query($args);
    
    return $posts;
}

/*
    Add post meta
*/
    add_action('admin_init', 'tu_add_post_meta_ground');
    function tu_add_post_meta_ground() {
        /** Meta box for general information */
        function tu_display_post_meta_ground($post) {
            $post_id = $post->ID;

            echo '<input type="hidden" name="nonce" value="' . wp_create_nonce('save_meta_box_ground') . '">';

            // $g_clearance = get_post_meta($post_id, 'g_clearance', true);
            $g_balcony = get_post_meta($post_id, 'g_balcony', true);
            $g_photo2d = get_post_meta($post_id, 'g_photo2d', true);  
            $g_photo3d = get_post_meta($post_id, 'g_photo3d', true);
            $g_code = get_post_meta($post_id, 'g_code', true );
            $g_name = get_post_meta($post_id, 'g_name', true );

            $metaboxData = array (
                // array (
                //     'label' => __('Nội dung tooltip', TEXT_DOMAIN),
                //     'name' => 'g_clearance',
                //     'value' => $g_clearance,
                //     'type' => 'editor'
                // ),
                array (
                    'label' => __('Tên loại hình căn', TEXT_DOMAIN),
                    'name' => 'g_name',
                    'value' => $g_name,
                    'type' => 'input',
                ),
                array (
                    'label' => __('Mã loại căn', TEXT_DOMAIN),
                    'name' => 'g_code',
                    'value' => $g_code,
                    'type' => 'input',
                ),
                array (
                    'label' => __('Mô tả ngắn', TEXT_DOMAIN),
                    'name' => 'g_balcony',
                    'value' => $g_balcony,
                    'type' => 'editor',
                ),
                array (
                    'label' => __('Ảnh 2D', TEXT_DOMAIN),
                    'name' => 'g_photo2d',
                    'value' => $g_photo2d,
                    'type'  => 'upload-image-album'
                ), 
                array (
                    'label' => __('Ảnh 3D', TEXT_DOMAIN),
                    'name' => 'g_photo3d',
                    'value' => $g_photo3d,
                    'type' => 'upload-image-album',
                ),
                
            );

            metaboxPrint($metaboxData);

        }

        add_meta_box (
            'tu_display_post_meta_ground', __('Thông tin căn hộ', TEXT_DOMAIN), 'tu_display_post_meta_ground', 'ground', 'normal', 'high'
        );
    }

    add_action('save_post', 'tu_save_post_meta_ground');
    function tu_save_post_meta_ground($post_id) {
        if (get_post_type() == 'ground' && isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'save_meta_box_ground')) {

            // if ( isset($_POST['g_clearance']) ) {
            //     update_post_meta($post_id, 'g_clearance', sanitize_text_field($_POST['g_clearance']));
            // } 

            if ( isset($_POST['g_balcony']) ) {
                update_post_meta($post_id, 'g_balcony', ($_POST['g_balcony']));
            } 

            if ( isset($_POST['g_photo2d'] ) ) {
                update_post_meta($post_id, 'g_photo2d', $_POST['g_photo2d']);
            }else {
                delete_post_meta($post_id, 'g_photo2d');
            }

            if ( isset($_POST['g_photo3d'] ) ) {
                update_post_meta($post_id, 'g_photo3d', $_POST['g_photo3d']);
            }else {
                delete_post_meta($post_id, 'g_photo3d');
            }

            if ( isset($_POST['g_code']) ) {
                update_post_meta($post_id, 'g_code', sanitize_text_field($_POST['g_code']));
            }

            if ( isset($_POST['g_name']) ) {
                update_post_meta($post_id, 'g_name', sanitize_text_field($_POST['g_name']));
            }

        }
    }

