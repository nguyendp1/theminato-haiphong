<?php

/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_ld_subdivision');
function tu_reg_post_type_ld_subdivision() {
    //Change this when creating post type
    $post_type_name = __('Quản lý các tiểu khu', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', '', 'thumbnail', ''),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('ld_subdivision', $args);

    /** Register Taxonomies */
    register_taxonomy('ld_subdivision_category', array('ld_subdivision' , 'untilites'), array(
        "hierarchical" => true,
        "label" => __('Chuyên mục tiểu khu', TEXT_DOMAIN),
        "singular_label" => __('Chuyên mục tiểu khu', TEXT_DOMAIN),
        "rewrite" => array('slug' => __('tieu-khu', TEXT_DOMAIN), 'hierarchical' => true),
        "show_admin_column" => true
    ));
    
}

function tu_get_ld_subdivision_with_pagination($page = 1, $post_per_page = 10, $custom_args = array()) {
    $args = array(
        'post_type' => 'ld_subdivision',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );

    // Push Taxonomy
    if (isset($custom_args['ld_subdivision_category'])) {

        array_push($args['tax_query'], array(
            'taxonomy' => 'ld_subdivision_category',
            'field' => 'id',
            'terms' => $custom_args['ld_subdivision_category']
        ));

        unset($custom_args['ld_subdivision_category']);
    }

    $args = array_merge($args, $custom_args);
    
    $posts = new WP_Query($args);

    return $posts;
}

/**
 * POST META BOXES ----------- ----------- -----------
 */


add_action('admin_init', 'tu_add_meta_box_ld_subdivision');
function tu_add_meta_box_ld_subdivision() {

    function tu_display_meta_box_ld_subdivision($post) {
        $post_id = $post->ID;
        $apartment_code = get_post_meta($post_id, 'apartment_code', true);
        ?>
        <table class="form-table">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('save_meta_box_ld_subdivision'); ?>">
            <tbody>
                <tr>
                    <th scope="row"><label for="apartment_code">Mã căn hộ</label></th>
                    <td>
                        <input type="text" class="regular-text" name="apartment_code" value="<?php echo $apartment_code; ?>">
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('tu_display_meta_box_ld_subdivision', 'Thông tin', 'tu_display_meta_box_ld_subdivision', 'ld_subdivision', 'normal', 'high');

}

add_action('save_post', 'tu_save_meta_ld_subdivision');
function tu_save_meta_ld_subdivision($post_id) {
    if ( isset( $_POST['apartment_code'] ) ) {
        update_post_meta($post_id, 'apartment_code', $_POST['apartment_code']);
    }
}

/**
 * Term metabox ld_subdivision_category in ground
 */
add_action('ld_subdivision_category_edit_form_fields', 'display_metabox_ld_subdivision_category', 99, 2 );
function display_metabox_ld_subdivision_category( $term ){
    $term_id = $term->term_id;
    $subdivision_code = get_term_meta($term_id, 'subdivision_code', true);
    $frame_1_title_1 = get_term_meta($term_id, 'frame_1_title_1', true);
    $frame_1_title_2 = get_term_meta($term_id, 'frame_1_title_2', true);
    $frame_2_title_1 = get_term_meta($term_id, 'frame_2_title_1', true);
    $frame_2_title_2 = get_term_meta($term_id, 'frame_2_title_2', true);
    $frame_2_desc = get_term_meta($term_id, 'frame_2_desc', true);
    $untilites_link = get_term_meta($term_id, 'untilites_link', true);
    ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><label for="subdivision_code">Mã tiểu khu</label></th>
                <td>
                    <input type="text" class="regular-text" name="subdivision_code" value="<?php echo $subdivision_code; ?>">
                </td>
            </tr>
        </tbody>
    </table>
    <?php tu_render_image_array_by_term_id_and_name('Banner', $term_id, 'banner'); ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><label for="frame_1_title_1">Frame 1 - Tiêu đề 1</label></th>
                <td>
                    <input type="text" class="regular-text" name="frame_1_title_1" value="<?php echo $frame_1_title_1; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="frame_1_title_2">Frame 1 - Tiêu đề 2</label></th>
                <td>
                    <input type="text" class="regular-text" name="frame_1_title_2" value="<?php echo $frame_1_title_2; ?>">
                </td>
            </tr>
        </tbody>
    </table>
    <?php tu_render_image_array_by_term_id_and_name('Fram 1 - Ảnh', $term_id, 'frame_1_image'); ?>
    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><label for="frame_2_title_1">Frame 2 - Tiêu đề 1</label></th>
                <td>
                    <input type="text" class="regular-text" name="frame_2_title_1" value="<?php echo $frame_2_title_1; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="frame_2_title_2">Frame 2 - Tiêu đề 2</label></th>
                <td>
                    <input type="text" class="regular-text" name="frame_2_title_2" value="<?php echo $frame_2_title_2; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="frame_2_desc">Frame 2 - Mô tả</label></th>
                <td>
                    <?php wp_editor($frame_2_desc, 'frame_2_desc', array('wpautop' => true, 'textarea_name' => 'frame_2_desc', 'textarea_rows' => 5, 'media_buttons' => false)); ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php tu_render_image_array_by_term_id_and_name('Fram 2 - Ảnh', $term_id, 'frame_2_image'); ?>
    <?php tu_render_image_array_by_term_id_and_name('Ảnh tiện ích tiểu khu', $term_id, 'slide_image'); ?>
    <?php
}


/*
    Save metabox taxonomy ld_subdivision_category
*/
    add_action('create_ld_subdivision_category', 'save_metabox_ld_subdivision_category', 10, 2 );
    add_action('edited_ld_subdivision_category', 'save_metabox_ld_subdivision_category', 10, 2 );
    function save_metabox_ld_subdivision_category( $term_id ){
        if (isset($_POST['subdivision_code'])) {
            update_term_meta($term_id, 'subdivision_code', $_POST['subdivision_code']);
        }
        if (isset($_POST['banner'])) {
            update_term_meta($term_id, 'banner', $_POST['banner']);
        }
        if (isset($_POST['frame_1_title_1'])) {
            update_term_meta($term_id, 'frame_1_title_1', $_POST['frame_1_title_1']);
        }
        if (isset($_POST['frame_1_title_2'])) {
            update_term_meta($term_id, 'frame_1_title_2', $_POST['frame_1_title_2']);
        }
        if (isset($_POST['frame_1_image'])) {
            update_term_meta($term_id, 'frame_1_image', $_POST['frame_1_image']);
        }
        if (isset($_POST['frame_2_title_1'])) {
            update_term_meta($term_id, 'frame_2_title_1', $_POST['frame_2_title_1']);
        }
        if (isset($_POST['frame_2_title_2'])) {
            update_term_meta($term_id, 'frame_2_title_2', $_POST['frame_2_title_2']);
        }
        if (isset($_POST['frame_2_desc'])) {
            update_term_meta($term_id, 'frame_2_desc', $_POST['frame_2_desc']);
        }
        if (isset($_POST['frame_2_image'])) {
            update_term_meta($term_id, 'frame_2_image', $_POST['frame_2_image']);
        }
        if (isset($_POST['slide_image'])) {
            update_term_meta($term_id, 'slide_image', $_POST['slide_image']);
        }
    }






