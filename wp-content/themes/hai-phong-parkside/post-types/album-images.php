<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_album_images');
function tu_reg_post_type_album_images() {
    //Change this when creating post type
    $post_type_name = __('Thư viện ảnh', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-welcome-widgets-menus',
        'supports' => array('title', 'thumbnail'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('album_images', $args);
    
}

function tu_get_album_images_with_pagination($page = 1, $post_per_page = 10) {
    $args = array(
        'post_type' => 'album_images',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );


    $posts = new WP_Query($args);

    return $posts;
}

/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_album_images');
function add_metabox_album_images() {
    function display_metabox_album_images($post) {
        $post_id = $post->ID;
        ?>
            <?php tu_render_image_array_by_post_id_and_name('album ảnh', $post_id, 'album_img'); ?>
        <?php
    }
    add_meta_box('display_metabox_album_images', 'Thư viện ảnh', 'display_metabox_album_images', 'album_images', 'normal', 'high');
}
 /**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_album_images');

function save_metabox_album_images($post_id) {

    if (isset($_POST['album_img'])) {
        update_post_meta($post_id, 'album_img', $_POST['album_img']);
    } 

}


