<?php
/**
 * INITIALIZE ----------- ----------- -----------
 */
add_action('init', 'tu_reg_post_type_home');
function tu_reg_post_type_home() {
    //Change this when creating post type
    $post_type_name = __('Quản lý trang chủ', TEXT_DOMAIN);
    $post_type_name_lower = mb_strtolower($post_type_name, 'utf-8');
    $post_type_name_slug = tu_remove_accent($post_type_name, '-');
    $post_type_menu_position = 3;

    $labels = array(
        'name' => $post_type_name,
        'singular_name' => $post_type_name,
        'menu_name' => $post_type_name,
        'all_items' => __('Tất cả', TEXT_DOMAIN).' '.$post_type_name_lower,
        'add_new' => __('Thêm mới', TEXT_DOMAIN),
        'add_new_item' => __('Thêm mới', TEXT_DOMAIN).' '.$post_type_name_lower,
        'edit_item' => __('Chỉnh sửa', TEXT_DOMAIN).' '.$post_type_name_lower,
        'new_item' => $post_type_name,
        'view_item' => __('Xem chi tiết', TEXT_DOMAIN),
        'search_items' => __('Tìm kiếm', TEXT_DOMAIN),
        'not_found' => __('Không tìm thấy bản ghi nào', TEXT_DOMAIN),
        'not_found_in_trash' => __('Không có bản ghi nào trong thùng rác', TEXT_DOMAIN),
        'view' => __('Xem', TEXT_DOMAIN).' '.$post_type_name_lower,
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_in_nav_menus' => false,
        'show_ui' => true,

        //Change this when creating post type
        'description' => $post_type_name,
        'menu_position' => $post_type_menu_position,
        'menu_icon' => 'dashicons-admin-home',
        'supports' => array('title', 'thumbnail'),
        'rewrite' => array(
            'slug' => $post_type_name_slug
        ),

        //Use `Page Template` instead, it is more easy to custom
        'has_archive' => false
    );

    register_post_type('home', $args);
    
}

function tu_get_home_with_pagination($page = 1, $post_per_page = 10) {
    $args = array(
        'post_type' => 'home',
        //'order' => 'DESC',
        //'orderby' => 'ID',
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_status' => 'publish',
        'tax_query' => array()
    );


    $posts = new WP_Query($args);

    return $posts;
}


/**
 * ADDING META BOXES
 */
add_action('admin_init', 'add_metabox_home');
function add_metabox_home() {
    // home-banner
    function display_metabox_home_banner($post) {
        $post_id = $post->ID;
        $home_banner_title1 = get_post_meta($post_id, 'home_banner_title1', true);
        $home_banner_title2 = get_post_meta($post_id, 'home_banner_title2', true);
        $home_banner_title3 = get_post_meta($post_id, 'home_banner_title3', true);
        $home_banner_link = get_post_meta($post_id, 'home_banner_link', true);
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="home_banner_title1">Tiêu đề 1</label></th>
                <td>
                    <input type="text" class="regular-text" name="home_banner_title1" value="<?php echo $home_banner_title1; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="home_banner_title2">Tiêu đề 2</label></th>
                <td>
                    <input type="text" class="regular-text" name="home_banner_title2" value="<?php echo $home_banner_title2; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="home_banner_title3">Tiêu đề 3</label></th>
                <td>
                    <input type="text" class="regular-text" name="home_banner_title3" value="<?php echo $home_banner_title3; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="home_banner_link">Liên kết</label></th>
                <td>
                    <input type="text" class="regular-text" name="home_banner_link" value="<?php echo $home_banner_link; ?>">
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('display_metabox_home_banner', 'Banner', 'display_metabox_home_banner', 'home', 'normal', 'high');
    // home-section3
    function display_metabox_home_s3($post) {
        $post_id = $post->ID;
        $home_s3_title1 = get_post_meta($post_id, 'home_s3_title1', true);
        $home_s3_title2 = get_post_meta($post_id, 'home_s3_title2', true);
        $home_s3_excerpt = get_post_meta($post_id, 'home_s3_excerpt', true);
        $home_s3_content = get_post_meta($post_id, 'home_s3_content', true);
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="home_s3_title1">Tiêu đề 1</label></th>
                <td>
                    <input type="text" class="regular-text" name="home_s3_title1" value="<?php echo $home_s3_title1; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="home_s3_title2">Tiêu đề 2</label></th>
                <td>
                    <input type="text" class="regular-text" name="home_s3_title2" value="<?php echo $home_s3_title2; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="home_s3_excerpt">Mô tả</label></th>
                <td>
                    <input type="text" class="regular-text" name="home_s3_excerpt" value="<?php echo $home_s3_excerpt; ?>">
                </td>
            </tr>
            <tr>
                <th scope="row"><label for="home_s3_content">Nội dung</label></th>
                <td>
                   <?php wp_editor($home_s3_content, 'home_s3_content', array('media_buttons' => false, 'textarea_name' => 'home_s3_content', 'textarea_rows' => 10)); ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('display_metabox_home_s3', 'Section3 - Vị trí dự án', 'display_metabox_home_s3', 'home', 'normal', 'high');
    // home-section4
    function display_metabox_home_s4($post) {
        $post_id = $post->ID;
        $home_s4_excerpt = get_post_meta($post_id, 'home_s4_excerpt', true);
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="home_s4_excerpt">Nội dung</label></th>
                <td>
                   <?php wp_editor($home_s4_excerpt, 'home_s4_excerpt', array('media_buttons' => false, 'textarea_name' => 'home_s4_excerpt', 'textarea_rows' => 5)); ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('display_metabox_home_s4', 'Section4 - Loại hình sản phẩm', 'display_metabox_home_s4', 'home', 'normal', 'high');

    // home-section5
    function display_metabox_home_s5($post) {
        $post_id = $post->ID;
        $home_s5_excerpt = get_post_meta($post_id, 'home_s5_excerpt', true);
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="home_s5_excerpt">Nội dung</label></th>
                <td>
                   <?php wp_editor($home_s5_excerpt, 'home_s5_excerpt', array('media_buttons' => false, 'textarea_name' => 'home_s5_excerpt', 'textarea_rows' => 5)); ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('display_metabox_home_s5', 'Section5 - Tiện ích', 'display_metabox_home_s5', 'home', 'normal', 'high');

     // home-section7
    function display_metabox_home_s7($post) {
        $post_id = $post->ID;
        $home_s7_excerpt = get_post_meta($post_id, 'home_s7_excerpt', true);
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="home_s7_excerpt">Nội dung</label></th>
                <td>
                   <?php wp_editor($home_s7_excerpt, 'home_s5_excerpt', array('media_buttons' => false, 'textarea_name' => 'home_s7_excerpt', 'textarea_rows' => 5)); ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }

    add_meta_box('display_metabox_home_s7', 'Section7 - Tin tức', 'display_metabox_home_s7', 'home', 'normal', 'high');

    // home-section8
    function display_metabox_home_s8($post) {
        $post_id = $post->ID;
        $home_s8_excerpt = get_post_meta($post_id, 'home_s8_excerpt', true);
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="home_s8_excerpt">Nội dung</label></th>
                <td>
                   <?php wp_editor($home_s8_excerpt, 'home_s8_excerpt', array('media_buttons' => false, 'textarea_name' => 'home_s8_excerpt', 'textarea_rows' => 5)); ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }
    add_meta_box('display_metabox_home_s8', 'Section8 - Thư viện dự án', 'display_metabox_home_s8', 'home', 'normal', 'high');

}


/**
 * Saving meta box information
 */
add_action('save_post', 'save_metabox_home');

function save_metabox_home($post_id) {

    /*banner*/
    if (isset($_POST['home_banner_title1'])) {
        update_post_meta($post_id, 'home_banner_title1', $_POST['home_banner_title1']);
    }

    if (isset($_POST['home_banner_title2'])) {
        update_post_meta($post_id, 'home_banner_title2', $_POST['home_banner_title2']);
    }

    if (isset($_POST['home_banner_title3'])) {
        update_post_meta($post_id, 'home_banner_title3', $_POST['home_banner_title3']);
    }

    if (isset($_POST['home_banner_link'])) {
        update_post_meta($post_id, 'home_banner_link', $_POST['home_banner_link']);
    }

    // Section3

    if (isset($_POST['home_s3_title1'])) {
        update_post_meta($post_id, 'home_s3_title1', $_POST['home_s3_title1']);
    }

    if (isset($_POST['home_s3_title2'])) {
        update_post_meta($post_id, 'home_s3_title2', $_POST['home_s3_title2']);
    }

    if (isset($_POST['home_s3_excerpt'])) {
        update_post_meta($post_id, 'home_s3_excerpt', $_POST['home_s3_excerpt']);
    }

    if (isset($_POST['home_s3_content'])) {
        update_post_meta($post_id, 'home_s3_content', $_POST['home_s3_content']);
    }
    // Section4
    if (isset($_POST['home_s4_excerpt'])) {
        update_post_meta($post_id, 'home_s4_excerpt', $_POST['home_s4_excerpt']);
    }
    // Section5
    if (isset($_POST['home_s5_excerpt'])) {
        update_post_meta($post_id, 'home_s5_excerpt', $_POST['home_s5_excerpt']);
    }

    // Section5
    if (isset($_POST['home_s7_excerpt'])) {
        update_post_meta($post_id, 'home_s7_excerpt', $_POST['home_s7_excerpt']);
    }

    // Section8
    if (isset($_POST['home_s8_excerpt'])) {
        update_post_meta($post_id, 'home_s8_excerpt', $_POST['home_s8_excerpt']);
    }
  
}




