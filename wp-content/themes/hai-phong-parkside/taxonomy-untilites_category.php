<?php 
get_header(); 
$untilities = tu_get_untilites_with_pagination(1, -1);
$terms_untilities = tu_get_terms_by_parent_id( 'untilites_category', 0);
$current_term_id = get_queried_object()->term_id;
$current_term_name = get_queried_object()->name;
$term_posts = tu_get_untilites_with_pagination(1, -1, array('untilites_category' => $current_term_id));
$untilites_category_banner = get_term_meta($current_term_id, 'untilites_category_banner', true);
$untilites_category_slide = get_term_meta($current_term_id, 'untilites_category_slide', true);
$untilites_category_des = get_term_meta($current_term_id, 'untilites_category_des', true);
?>

<div class="tien-ich">
	<?php if ( isset( $untilites_category_banner ) && $untilites_category_banner ) : ?>
		<?php foreach ( $untilites_category_banner as $image ) : ?>
			<?php
			$image_id = $image['id'];
			$image_src = tu_get_image_src_by_attachment_id($image_id, '');
			?>
			<div class="baner" style="background-image: url('<?php echo $image_src; ;?>');"></div>
		<?php endforeach; ?>
	<?php endif; ?>
	<div class="precent">
		<h1>Tiện ích dự án</h1>
		<div class="_desc">
			<?php echo apply_filters('the_content', $untilites_category_des); ?>
		</div>
	</div>
	<div class="tieukhu">
		<div class="menu-tab-tieukhu">
			<ul>
				<?php foreach ( $terms_untilities as $term ) : ?>
					<?php
					$terms_untilities_id = $term->term_id;
					$term_slug = $term->slug;
					$terms_untilities_name = $term->name;
					$term_link = get_term_link($terms_untilities_id);
					$active = $current_term_id == $terms_untilities_id ? 'active-menu-tieukhu' : '' ;
					?>
					<li><a href="<?php echo $term_link ?>" class="<?php echo $active; ?>"><?php echo $terms_untilities_name ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<div class="hermes">
		<div class="map-hermes">
			<div class="img-map">
				<img src="<?php echo IMAGE_URL . '/tienich/maptong.jpg' ;?>" alt="">
			</div>
			<div class="list">
				<?php
				if ($term_posts->have_posts()) :
					while ($term_posts->have_posts()) :
						$term_posts->the_post();
						$post_id = $post->ID;
						$number = get_the_excerpt($post_id);
						$title = get_the_title($post_id);
						?>
						<a href="javascript:void(0);" data-order="<?php echo $number; ?>">
							<?php echo $number; ?>
							<p><?php echo $title; ?></p>
						</a>
						<?php
					endwhile;
				endif;
				?>
			</div>
			<div class="poin">
				<div class="maps-min">
					<?php foreach ( $terms_untilities as $term ) : ?>
						<?php
						$terms_untilities_id = $term->term_id;
						$term_slug = $term->slug;
						$terms_untilities_name = $term->name;
						$term_link = get_term_link($terms_untilities_id);
						$active = $current_term_id == $terms_untilities_id ? 'active-maps-min' : '' ;
						?>
						<a href="<?php echo $term_link; ?>" class="<?php echo $active; ?>" data-src="<?php echo $term_slug; ?>">
							<img src="<?php echo IMAGE_URL . '/tienich/' ;?><?php echo $term_slug; ?>.png" alt="">
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="row-tieukhu">
			<div class="name-tab">
				<div>
					<h1>tiện ích</h1>
					<h2><?php echo $current_term_name;?></h2>
				</div>
			</div>
			<div class="list-tieukhu">
				<div>
					<?php 
					$ground = tu_get_untilites_with_pagination(1, -1, array('untilites_category' => $current_term_id));
					if ( $ground->have_posts() ) : ?>
						<?php while ( $ground->have_posts() ) : $ground->the_post(); ?>
							<?php
							$untilities_id = get_the_ID();
							$title = get_the_title($untilities_id);
							$number = get_the_excerpt($untilities_id);
							?>
							<p data-order="<?php echo $number; ?>"><?php  echo $number; ?>. <?php  echo $title; ?></p>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="slide-tieukhu">
			<div class="swiper-container js-slide-hermes">
				<div class="swiper-wrapper">
					<?php if ( isset( $untilites_category_slide ) && $untilites_category_slide ) : ?>
						<?php foreach ( $untilites_category_slide as $image ) : ?>
							<?php
							$image_id = $image['id'];
							$image_src = tu_get_image_src_by_attachment_id($image_id, '');
							?>
							<div class="swiper-slide">
								<div class="con-slide" style="background-image: url(<?php echo $image_src; ;?>);">
									<div></div>
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>

				</div>
				<div class="activ-slide-hermes">
					<div class="swiper-button-next js-next-hermes">
						<img src="<?php echo IMAGE_URL . '/tienich/next_w.png' ;?>" alt="">
					</div>
					<div class="swiper-button-prev js-prev-hermes">
						<img src="<?php echo IMAGE_URL . '/tienich/prev_w.png' ;?>" alt="">
					</div>
					<div class="swiper-pagination js-paginaton-hermes"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function($){
		var slide_tieukhu = new Swiper('.js-slide-hermes',{
			slidesPerView: 1,
			// spaceBetween: 22,
			pagination: {
				el: '.js-paginaton-hermes',
				clickable: true,
				type: 'bullets',
				dynamicBullets: false,
				dynamicMainBullets: 2,
			},
			navigation: {
				nextEl: '.js-next-hermes',
				prevEl: '.js-prev-hermes',
			},
		});

		$('.tien-ich .hermes .row-tieukhu .list-tieukhu ul li p').click(function(event){
			// event.preventDefault();
			// $('html').animate({ scrollTop: 900},500);
			var hermes_top = $('.hermes').offset().top - $('.header').outerHeight();

			$('html').animate({ scrollTop: hermes_top},500);
		});

		$('[data-order]').hover(function(){
			var stt = $(this).attr('data-order');
			$('[data-order="'+ stt +'"]').addClass('active--is');

		}, function(){
			$('.list-tieukhu p').removeClass('active--is');
			$('.map-hermes .list a').removeClass('active--is');
		});
	});
</script>
<?php get_footer(); ?>