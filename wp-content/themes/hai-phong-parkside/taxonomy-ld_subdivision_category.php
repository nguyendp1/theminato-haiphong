<?php 
get_header(); 
$current_term_id = get_queried_object()->term_id;
$current_term_name = get_queried_object()->slug;
$subdivision_code = get_term_meta($current_term_id, 'subdivision_code', true);
$banner = get_term_meta($current_term_id, 'banner', true);
$frame_1_title_1 = get_term_meta($current_term_id, 'frame_1_title_1', true);
$frame_1_title_2 = get_term_meta($current_term_id, 'frame_1_title_2', true);
$frame_1_desc = get_queried_object()->description;
$frame_1_image = get_term_meta($current_term_id, 'frame_1_image', true);
$frame_2_title_1 = get_term_meta($current_term_id, 'frame_2_title_1', true);
$frame_2_title_2 = get_term_meta($current_term_id, 'frame_2_title_2', true);
$frame_2_desc = get_term_meta($current_term_id, 'frame_2_desc', true);
$frame_2_image = get_term_meta($current_term_id, 'frame_2_image', true);
$slide_image = get_term_meta($current_term_id, 'slide_image', true);
$subdivision = tu_get_ld_subdivision_with_pagination(1, -1, array('ld_subdivision_category' => $current_term_id));
$untilites = tu_get_untilites_with_pagination(1, -1, array('ld_subdivision_category' => $current_term_id ));
?>
<div class="landing-page">
	<?php if ( isset( $banner ) && $banner ) : ?>
		<?php foreach ( $banner as $image ) : ?>
			<?php
			$image_id = $image['id'];
			$image_src = tu_get_image_src_by_attachment_id($image_id, '');
			?>
			<div class="baner" style="background-image: url(<?php echo $image_src; ?>);"></div>
		<?php endforeach; ?>
	<?php endif; ?>
	<div class="section_1" style="background-image: url(<?php echo IMAGE_URL . '/landing-page/luoi.jpg' ;?>);">
		<div class="tong-quan">
			<div class="title_lable">
				<h1><?php echo $frame_1_title_1; ?></h1>
				<h2><?php echo $frame_1_title_2; ?></h2>
				<div><?php echo apply_filters('the_content', $frame_1_desc); ?></div>
			</div>
		</div>
		<div class="tong-quan-image">
			<?php if ( isset( $frame_1_image ) && $frame_1_image ) : ?>
				<?php foreach ( $frame_1_image as $image ) : ?>
					<?php
					$image_id = $image['id'];
					$image_src = tu_get_image_src_by_attachment_id($image_id, '');
					?>
					<img src="<?php echo $image_src;?>" alt="">
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	<div class="section_2" style="background-image: url(<?php echo IMAGE_URL . '/landing-page/vitri-luoi.jpg' ;?>);">
		<div class="vi-tri-image">
			<?php if ( isset( $frame_2_image ) && $frame_2_image ) : ?>
				<?php foreach ( $frame_2_image as $image ) : ?>
					<?php
					$image_id = $image['id'];
					$image_src = tu_get_image_src_by_attachment_id($image_id, '');
					?>
					<img src="<?php echo $image_src; ?>" alt="">
				<?php endforeach; ?>
			<?php endif; ?>
			
		</div>
		<div class="vi-tri">
			<div class="title_lable">
				<h1><?php echo $frame_2_title_1;?></h1>
				<h3><?php echo $frame_2_title_2;?></h3>
				<div><?php echo apply_filters('the_content', $frame_2_desc); ?></div>
			</div>
		</div>
	</div>
	<div class="section_3">
		<div class="loai-hinh">
			<div class="title_lable">
				<h1>loại hình dự án</h1>
			</div>
		</div>
		<div class="loai-hinh-image">
			<div class="swiper-container" id="js-loai-hinh-slide">
				<div class="swiper-wrapper">
					<?php if ( $subdivision->have_posts() ) : ?>
						<?php while ( $subdivision->have_posts() ) : $subdivision->the_post(); ?>
							<?php
							$term_id = $post->ID;
							$title = get_the_title($term_id);
							$thumbnail_url = get_the_post_thumbnail_url( $term_id, 'full' );
							$apartment_code = get_post_meta($term_id, 'apartment_code', true);
							?>
							<div class="swiper-slide">
								<div class="img-backg" style="background-image: url(<?php echo $thumbnail_url; ?>);">
									<div class="backg-black"></div>
									<div class="name-loai-hinh"><?php echo $title; ?></div>
									<div class="link-chi-tiet">
										<a class="more" href="<?php echo HOME_URL; ?>tong-mat-bang/?subdivision=<?php echo $subdivision_code;?>&apartment=<?php echo $apartment_code;?>">xem chi tiết căn</a>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="swiper-button-next js-next-section3-slide">
				<img src="<?php echo IMAGE_URL . '/landing-page/next_black.png';?>" alt="">
			</div>
			<div class="swiper-button-prev js-prev-section3-slide">
				<img src="<?php echo IMAGE_URL . '/landing-page/prev_black.png';?>" alt="">
			</div>
		</div>
	</div>
	<div class="section_4">
		<div class="tien-ich" style="background-image: url(<?php echo IMAGE_URL . '/landing-page/vitri-luoi.jpg' ;?>);">
			<div class="title_lable">
				<h1>Tiện ích nội khu</h1>
			</div>
			<div class="to-slide-tien-ich">
				<?php if ( isset( $untilites ) && $untilites ) : $i=0;?>
					<?php while ( $untilites->have_posts() ) : $untilites->the_post(); ?>
						<?php
						$i++;
						$post_id = get_the_ID();
						$title = get_the_title($post_id);
						?>
						<p onclick="slide_tienich.slideTo(<?php echo $i - 1; ?>)"><span><?php echo $i;?></span><?php echo $title; ?></p>
					<?php endwhile; ?>
				<?php endif; ?>
				<a class="more" href="<?php echo HOME_URL; ?>tien-ich/<?php echo $current_term_name; ?>">xem sơ đồ tiện ích</a>
			</div>
		</div>
		<div class="swiper-container" id="js-slide-tien-ich">
			<div class="swiper-wrapper">
				<?php if ( isset( $slide_image ) && $slide_image ) : ?>
					<?php foreach ( $slide_image as $image ) : ?>
						<?php
						$image_id = $image['id'];
						$image_src = tu_get_image_src_by_attachment_id($image_id, '');
						?>
						<div class="swiper-slide">
							<div class="image_tien-ich" style="background-image: url(<?php echo $image_src; ?>);">
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="swiper-button-next js-next-tien-ich">
				<img src="<?php echo IMAGE_URL . '/landing-page/next_w.png' ;?>" alt="">
			</div>
			<div class="swiper-button-prev js-prev-tien-ich">
				<img src="<?php echo IMAGE_URL . '/landing-page/prev_w.png' ;?>" alt="">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var slide_tienich;
	jQuery(document).ready(function($){
		slide_tienich = new Swiper('#js-slide-tien-ich',{
			navigation: {
				nextEl: '.js-next-tien-ich',
				prevEl: '.js-prev-tien-ich',
			},
		});

		var slide_loaihinh = new Swiper('#js-loai-hinh-slide',{
			slidesPerView: 4,
			spaceBetween: 22,
			navigation: {
				nextEl: '.js-next-section3-slide',
				prevEl: '.js-prev-section3-slide',
			},
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 10
				},
				480: {
					slidesPerView: 1,
					spaceBetween: 0
				},
				768: {
					slidesPerView: 2,
					spaceBetween: 22
				}
			}
		});


	});
</script>
<?php get_footer(); ?>
