
<footer class="footer">
	<div class="item-footer"><img src="<?php echo IMAGE_URL .'/home/header-footer-s9/logo-footer.png'?>" alt=""></div>
	<div class="item-footer">
		<div class="title-footer">address</div>
		<div class="lineText">
			<i class="fa fa-map-marker"></i>
			<p> Near Cau Rao 2, Water Front City, Vinh Niem ward, Le Chan district, Hai Phong city </p>
		</div>
		<a href="tel:0901237456" class="lineText">
			<i class="fa fa-phone"></i>
			<p>090 1237 456 </p>
		</a>
		<a href="mailto:Sale@theminatoresidence.vn" class="lineText">
			<i class="fa fa-envelope"></i>
			<p>Sale@theminatoresidence.vn</p>
		</a>
	</div>
	<div class="item-footer">
		<div class="title-footer">TRANSACTION ADDRESS </div> 
		<a href="tel:0901237456" class="lineText">
			<i class="fa fa-phone"></i>
			<p>090 1237 456  </p>
		</a>
		<a href="mailto:Sale@theminatoresidence.vn" class="lineText">
			<i class="fa fa-envelope"></i>
			<p>Sale@theminatoresidence.vn</p>
		</a>
		<div class="lineText lineText-bottom">
			<p>Connect us: </p>
			<a href=""><i class="fa fa-facebook"></i></a> 
			<a href=""><i class="fa fa-youtube-play"></i> </a>
			<a href=""><i class="fa fa-google-plus"></i> </a>
		</div>
	</div>
	<div class="item-footer">
		<i class="fa fa-copyright"></i>Copyright © 2019 The Minato Residence. All Rights Reserved.
	</div>
</footer>
<?php include_once (TEMPLATE_PATH.'/partials/network-socials.php'); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		
	});
</script>
<?php wp_footer(); ?>

</body>
</html>
