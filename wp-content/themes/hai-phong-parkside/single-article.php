<?php
get_header();

while (have_posts()):
    the_post();
    $post_id = $post->ID;
    $article_thumbnail_url = get_the_post_thumbnail_url( $post_id, 'full' );
    $news_related = tu_get_article_with_pagination(1, 3, array( 'post__not_in' => array($post_id) ));
?>
   <div class="tin-tuc_chi-tiet" style="background-image: url(<?php echo IMAGE_URL . '/news/nen-luoi.png'; ?>);">
        <div class="tintuc-baner" style="background-image: url(<?php echo IMAGE_URL . '/news/banner.jpg'; ?>);"></div>
        <div class="chitiet_bark">
            <p class="ct-prev"><a href="<?php echo get_page_link(22); ?>"><span><i class="fa fa-angle-left" aria-hidden="true"></i> </span> trở lại danh sách</a></p>
            <h1><?php the_title(); ?></h1>
            <!-- <h2>nghệ thuật hưởng thụ cuộc sống</h2> -->
            <p class="ct_time"><span><i class="fa fa-clock-o" aria-hidden="true"></i></span><?php echo get_the_date('d.m.y'); ?></p>
        </div>
        <div class="the-content">
            <p><strong><?php the_excerpt(); ?></strong></p>
            <?php the_content(); ?>
            <div class="chitiet_ddan">
                <a href="#">#Lorem</a>
                <a href="#">#Ipsum</a>
            </div>
        </div>
        <div class="chitiet_tin-lien-quan">
            <h3>tin liên quan</h3>
            <?php if ($news_related->have_posts()) : ?>
                <?php while ($news_related->have_posts()) : $news_related->the_post(); ?>
                <?php
                    $post_id = get_the_ID();
                    $article_thumbnail_url = get_the_post_thumbnail_url( $post_id, 'full' );
                    $article_permalink = get_the_permalink($post_id);
                    $article_title = get_the_title($post_id);
                    $article_date = get_the_date('d/m/Y');
                ?>
                <a href="<?php echo $article_permalink; ?>" class="tin-lien-he_c1">
                    <div class="img_lien-he" style="background-image: url(<?php echo $article_thumbnail_url; ?>)">
                    </div>
                    <div class="lienhe_tt">
                        <h1><?php echo $article_title; ?></h1>
                        <p><span><i class="fa fa-clock-o" aria-hidden="true"></i></span><?php echo $article_date; ?></p>
                    </div>
                </a>
                <?php endwhile; ?>
            <?php endif; ?>
           
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>