<div class="section_2">
	<div class="col_left">
		<h2>the minato residence</h2>
		<h3>introduction</h3>
		<div class="wrap_icon">
			<div class="item_apiece">
				<div class="_icon">
					<img src="<?php echo IMAGE_URL .'/home/icon1.png'?>" alt="">
				</div>
				<div class="_itimate">
					Total area: <br>
					12,635 m2
				</div>
			</div>
			<div class="item_apiece">
				<div class="_icon">
					<img src="<?php echo IMAGE_URL .'/home/icon2.png'?>" alt="">
				</div>
				<div class="_itimate">
					Building density:<br>
					43%
				</div>
			</div>
			<div class="item_apiece">
				<div class="_icon">
					<img src="<?php echo IMAGE_URL .'/home/icon3.png'?>" alt="">
				</div>
				<div class="_itimate">
					Total units: <br>
					924
				</div>
			</div>
			<div class="item_apiece">
				<div class="_icon">
					<img src="<?php echo IMAGE_URL .'/home/icon5.png'?>" alt="">
				</div>
				<div class="_itimate">
				  2	Buildings: <br>
				  26 floors abouve ground, 1 basement
				</div>
			</div>
		</div>
		<div class="_content">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, nostrum, nihil illum rem aliquid aperiam! Commodi praesentium assumenda vitae, delectus reiciendis excepturi, officia quas aliquid culpa, itaque facilis maiores distinctio.
		</div>
	</div>
	<div class="col_right">
		<div class="slide_s2">
			<div class="swiper-container s2_container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="img_slide" style="background-image: url('<?php echo IMAGE_URL . '/home/slide_s2_1.png' ?>');">
							<h4>lorem ipsum the</h4>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide" style="background-image: url('<?php echo IMAGE_URL . '/home/slide_s2_2.png' ?>');">
							<h4>lorem ipsum the</h4>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide" style="background-image: url('<?php echo IMAGE_URL . '/home/slide_s2_1.png' ?>');">
							<h4>lorem ipsum the</h4>
						</div>
					</div>
				</div>
				<div class="swiper-button-next">
					<img src="<?php echo IMAGE_URL . '/home/next.png'?>" alt="">
				</div>
		    	<div class="swiper-button-prev">
		    		<img src="<?php echo IMAGE_URL . '/home/prev.png'?>" alt="">
		    	</div>		
			</div>
		</div>
	</div>
	<div class="swiper-pagination"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var slide_section2 = new Swiper('.section_2 .swiper-container', {
			slidesPerView: 2,
      		spaceBetween: 30,
      		loop: true,
		    pagination: {
		        el: '.section_2 .swiper-pagination',
		        clickable: true,
		    },
		    navigation: {
		        nextEl: '.section_2 .swiper-button-next',
		        prevEl: '.section_2 .swiper-button-prev',
		    },
		    breakpoints: {
			    320: {
			      slidesPerView: 1,
			      spaceBetween: 10
			    },
			    480: {
			      slidesPerView: 1.2,
			      spaceBetween: 15
			    },
			    1024: {
			      slidesPerView: 1.5,
			      spaceBetween: 30
			    }
			}
	    });
	});
</script>