<div class="section_10" style="display: none;" >
		<div class="swiper-container js_s10_container">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="img_slide" style="background-image: url('<?php echo IMAGE_URL . '/home/backg360.png' ?>');"></div>
					<div class="title">
						<h2>Shoping mall</h2>
						<p>The minato residence</p>
					</div>
					<div class="text">
						<h2>Perfect utilities</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident perferendis, optio sit ratione cupiditate assumenda quibusdam hic excepturi tenetur dolore.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="img_slide" style="background-image: url('<?php echo IMAGE_URL . '/home/backg360.png' ?>');"></div>
					<div class="title">
						<h2>Shoping mall</h2>
						<p>The minato residence</p>
					</div>
					<div class="text">
						<h2>Perfect utilities</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident perferendis, optio sit ratione cupiditate assumenda quibusdam hic excepturi tenetur dolore.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="img_slide" style="background-image: url('<?php echo IMAGE_URL . '/home/backg360.png' ?>');"></div>
					<div class="title">
						<h2>Shoping mall</h2>
						<p>The minato residence</p>
					</div>
					<div class="text">
						<h2>Perfect utilities</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident perferendis, optio sit ratione cupiditate assumenda quibusdam hic excepturi tenetur dolore.</p>
					</div>
				</div>
			</div>		
		</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var slide_section5 = new Swiper('.section_10 .js_s10_container', {
      		slidesPerView: 1,
      		speed: 800
	    });
	});
</script>