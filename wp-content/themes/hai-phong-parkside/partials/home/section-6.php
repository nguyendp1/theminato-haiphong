<div class="section_6">
	<div class="swiper-container s6-slide-container">
		<div class="swiper-wrapper">
			<div class="swiper-slide" data-slide='1'>
				<div class="img-slide" style="background-image: url('<?php echo IMAGE_URL . '/home/img-slide6.png'?>');">
					<h1>Shopping mall</h1>
					<p>THE MINATO RESIDENCE</p>
				</div>
			</div>
			<div class="swiper-slide" data-slide='2'>
				<div class="img-slide" style="background-image: url('<?php echo IMAGE_URL . '/home/img-slide6.png'?>');">
					<h1>Shopping mall</h1>
					<p>THE MINATO RESIDENCE</p>
				</div>
			</div>
			<div class="swiper-slide" data-slide='3'>
				<div class="img-slide" style="background-image: url('<?php echo IMAGE_URL . '/home/img-slide6.png'?>');">
					<h1>Shopping mall</h1>
					<p>THE MINATO RESIDENCE</p>
				</div>
			</div>
		</div>
	</div>
	<div class="perfect-fluid" style="background-image: url(<?php echo IMAGE_URL . '/home/backgPerfect.png'?>);">
		<div class="perfect">
			<div box-data='1' class="content-tex active-span">
				<h2>PERFECT UTILITIES1</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, type specimen book.</p>
			</div>
			<div box-data='2' class="content-tex">
				<h2>PERFECT UTILITIES2</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, type specimen book.</p>
			</div>
			<div box-data='3' class="content-tex">
				<h2>PERFECT UTILITIES3</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, type specimen book.</p>
			</div>
			<div class="_stt">
				<span box-data='1' class="active-span">01</span>
				<span box-data='2'>02</span>
				<span box-data='3'>03</span>
				<div class="swiper-button-next">
					<img src="<?php echo IMAGE_URL . '/home/nextb.png'?>" alt="">
				</div>
		    	<div class="swiper-button-prev">
		    		<img src="<?php echo IMAGE_URL . '/home/prevb.png'?>" alt="">
		    	</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var slide_s6 = new Swiper ('.s6-slide-container', {
			navigation: {
		        nextEl: '.section_6 .swiper-button-next',
		        prevEl: '.section_6 .swiper-button-prev',
		    },
		    autoplay: {
			    delay: 3000,
			},
		});
		slide_s6.on('slideChange', function(swiper){
            var index = slide_s6.activeIndex + 1;
			var box = '[box-data="' + index +'"]'; 
			console.log(box);

			$('.section_6 .perfect-fluid .perfect ._stt span ').removeClass('active-span');
			$('.section_6 .perfect-fluid .perfect .content-tex').removeClass('active-span');
			$(box).addClass('active-span');
        });
	});
</script>
