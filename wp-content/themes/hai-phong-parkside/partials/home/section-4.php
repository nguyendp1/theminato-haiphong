<div class="section_4">
	<div class="title_s4">
		floor plans<br>THE MINATO RESIDENCE
	</div>
	<div class="flat">
		<ul class="link_tab">
			<li data-src="tab1" class="active-m">m1</li>
			<li data-src="tab2">m2</li>
			<li data-src="tab3">m3</li>
		</ul>
	</div>
	<div class="wrapper-tab">
		<div data-src="tab1" class="slide_flat active-m" id="tab1">
			<div class="swiper-container s4-slide-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-button-next">
				<img src="<?php echo IMAGE_URL . '/home/next2.png'?>" alt="">
			</div>
	    	<div class="swiper-button-prev">
	    		<img src="<?php echo IMAGE_URL . '/home/prev2.png'?>" alt="">
	    	</div>	
			<div class="swiper-pagination"></div>
		</div>
		<div data-src="tab2" class="slide_flat" id="tab2">
			<div class="swiper-container s4-slide-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-button-next">
				<img src="<?php echo IMAGE_URL . '/home/next2.png'?>" alt="">
			</div>
	    	<div class="swiper-button-prev">
	    		<img src="<?php echo IMAGE_URL . '/home/prev2.png'?>" alt="">
	    	</div>	
			<div class="swiper-pagination"></div>
		</div>
		<div data-src="tab3" class="slide_flat" id="tab3">
			<div class="swiper-container s4-slide-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="img_slide">
							<p>Floor plan 1</p>
							<img src="<?php echo IMAGE_URL . '/home/slide_mb_1.png'?>" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-button-next">
				<img src="<?php echo IMAGE_URL . '/home/next2.png'?>" alt="">
			</div>
	    	<div class="swiper-button-prev">
	    		<img src="<?php echo IMAGE_URL . '/home/prev2.png'?>" alt="">
	    	</div>	
			<div class="swiper-pagination"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var slide_section2_tab1 = new Swiper('#tab1 .swiper-container', {
      		loop: true,
		    pagination: {
		        el: '#tab1 .swiper-pagination',
		        clickable: true,
		    },
		    navigation: {
		        nextEl: '#tab1 .swiper-button-next',
		        prevEl: '#tab1 .swiper-button-prev',
		    },
	    });
	    var slide_section2_tab2 = new Swiper('#tab2 .swiper-container', {
      		loop: true,
		    pagination: {
		        el: '#tab2 .swiper-pagination',
		        clickable: true,
		    },
		    navigation: {
		        nextEl: '#tab2 .swiper-button-next',
		        prevEl: '#tab2 .swiper-button-prev',
		    },
	    });
	    var slide_section2_tab3 = new Swiper('#tab3 .swiper-container', {
      		loop: true,
		    pagination: {
		        el: '#tab3 .swiper-pagination',
		        clickable: true,
		    },
		    navigation: {
		        nextEl: '#tab3 .swiper-button-next',
		        prevEl: '#tab3 .swiper-button-prev',
		    },
	    });
		$('.section_4 .flat .link_tab li').click(function(event){
			$('.section_4 .flat .link_tab li').removeClass('active-m');
			$('.section_4 .slide_flat').removeClass('active-m');
			var stt = $(this).attr('data-src');
			$('[data-src="'+ stt +'"]').addClass('active-m');
		});
	});
</script>