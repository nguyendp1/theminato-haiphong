<div class="section_5">
	<div class="title-s5">
		<h3>7 REASONS WHY WE SHOULD OWN<br>AN APARTMENT IN THE MINATO RESIDENCE</h3>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
	</div>
	<div class="slide-concepts">
		<div class="swiper-container s5-slide-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="slide-img">
						<img src="<?php echo IMAGE_URL . '/home/slide5_i1.png'?>" alt="">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, type specimen book.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="slide-img">
						<img src="<?php echo IMAGE_URL . '/home/slide5_i2.png'?>" alt="">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, type specimen book.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="slide-img">
						<img src="<?php echo IMAGE_URL . '/home/slide5_i3.png'?>" alt="">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, type specimen book.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="swiper-button-next">
			<img src="<?php echo IMAGE_URL . '/home/next2.png'?>" alt="">
		</div>
    	<div class="swiper-button-prev">
    		<img src="<?php echo IMAGE_URL . '/home/prev2.png'?>" alt="">
    	</div>	
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var slide_section5 = new Swiper('.section_5 .swiper-container', {
      		// loop: true,
      		slidesPerView: 3,
      		spaceBetween: 30,
		    navigation: {
		        nextEl: '.section_5 .swiper-button-next',
		        prevEl: '.section_5 .swiper-button-prev',
		    },
      		autoplay: {
			    delay: 3000,
			},
		    breakpoints: {
			    320: {
			      slidesPerView: 1,
			      spaceBetween: 10
			    },
			    480: {
			      slidesPerView: 2,
			      spaceBetween: 15
			    },
			    autoplay: {
				    delay: 1000,
				},
			}
	    });
	});
</script>