<form method="POST" class="contact-form js-contact-submit-def">
	<input type="hidden" name="action" value="submit_contact">
    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('submit_contact'); ?>">
    <div class="all-form-fields">
    	<p class="ground-form-title">Đăng ký mua căn hộ</p>
		<div class="form-group">
			<input type="text" class="form-control" id="contact_name" name="contact_name" value="" placeholder="Họ và tên">
		</div>
		<div class="form-group">
			<input type="tel" class="form-control" id="contact_phone" name="contact_phone" value="" placeholder="Số điện thoại">
		</div>
		<div class="form-group">
			<input type="text" class="form-control" id="contact_email" name="contact_email" value="" placeholder="Email">
		</div>
		<div class="form-group frm_box">
			<p>Sản phẩm tôi quan tâm?</p>
			<div class="_item">
				<input id="shophouse" type="radio" value="Shophouse" name="contact_type_product">
				<label for="shophouse" class="check">Shophouse</label>
			</div>
			<div class="_item">
				<input id="lien-ke" type="radio" value="Liền kề" name="contact_type_product">
				<label for="lien-ke" class="check">Liền kề</label>
			</div>
			<div class="_item">
				<input id="biet-thu" type="radio" value="Biệt thự" name="contact_type_product">
				<label for="biet-thu" class="check">Biệt thự</label>
			</div>
			<div class="_item">
				<input id="villa" type="radio" value="Shophouse Villa" name="contact_type_product">
				<label for="villa" class="check">Shop Villa</label>
			</div>
			<div class="_item">
				<input id="song-lap" type="radio" value="Biệt thự song lập" name="contact_type_product">
				<label for="song-lap" class="check">Biệt thự song lập</label>
			</div>
		</div>
		<div class="form-group">
			<textarea class="form-control" id="contact_content" name="contact_content" placeholder="Lời nhắn"></textarea>
		</div>
		<div class="form-group">
			<button type="submit" class="btn-submit" name="action">Đăng ký</button>
		</div>
	</div>
	<div class="form-msg"></div>
</form>


