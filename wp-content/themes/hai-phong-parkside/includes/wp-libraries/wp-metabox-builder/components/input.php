<?php

function metabox_builder_input($item = array()) {
    $label = $item['label'];
    $name = $item['name'];
    $value = $item['value'];
    ?>

     <table class="form-table">
        <tbody>
            <tr>
                <th scope="row">
                    <label for="orbit_content"><?php echo $label; ?></label>
                </th>
                <td>
                    <div class="field-input">
                        <input type="text" name="<?php echo $name; ?>" value="<?php echo $value; ?>">
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
    <?php
}