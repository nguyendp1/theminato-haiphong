<?php 

/*
* @function: metabox_editor
* @parameter: $item is array ( label, name, value )
* @note: This function hasn't validation and format
*/
function metabox_builder_editor($item = array()) {
    $label = $item['label'];
    $name = $item['name'];
    $value = $item['value'];
    ?>

    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row">
                    <label for="orbit_content"><?php echo $label; ?></label>
                </th>
                <td>
                    <div class="field-editor">
                        <?php wp_editor($value, $name); ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    
    <?php
}
