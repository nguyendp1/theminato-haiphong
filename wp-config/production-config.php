<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/**
 * Use this to set site url
 *
define('WP_HOME','http://example.com');
define('WP_SITEURL','http://example.com');
 */

/**
 * Use this to replace base url in database:
 *
UPDATE wp_posts SET post_content = REPLACE(post_content,'http://old','http://new');
UPDATE wp_options SET option_value = REPLACE(option_value,'http://old','http://new');
 */

/** The name of the database for WordPress */
define('DB_NAME', 'crownvilla_home');

/** MySQL database username */
define('DB_USER', 'crownvilla_home');

/** MySQL database password */
define('DB_PASSWORD', 'KYQ3xvbgJXFKQdW7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Disallow admin to edit plugins, theme */
define( 'DISALLOW_FILE_EDIT', true );
define( 'DISALLOW_FILE_MODS', true );

/** Disable auto update core */
define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 *
 * Use this to generate key: https://api.wordpress.org/secret-key/1.1/salt/
 */
define('AUTH_KEY',         'C5Bz$IpMd5D~S*-7g3j%~UJ-QLpgj a$U-]jRZa8K8)/g/CuEK|cCuS0@|QgsN0z');
define('SECURE_AUTH_KEY',  'g:3YpfjpF.byz[&h Hxs_d.E=HgW]^M:Sf+w,jzN4-6v^hT-cwBbep,R-h|5QU-#');
define('LOGGED_IN_KEY',    'N_q2uWW.vg[/YXr_yA%7gJrUTU4a@mCe-3m+466]G%sizrSQs]ynbGP iM,TPj[a');
define('NONCE_KEY',        'udKSVfcs{u`=Kvf}qU)4%%>J8|X~)$Z<MKADLJSobz9IE`Pogvv+9R.Qj`+tGk_i');
define('AUTH_SALT',        'c$%+p}gA2vv7L)6AlEWfi;brXIFcMP PJ3l3],S}Fx_2 [~6k2J.r@;a*/gCc!^H');
define('SECURE_AUTH_SALT', 'DF4&2CK*_DyjB> )O}1YG$x(#@{4oE+11oWNR>O/!pRia0dQLN=,w!T*8s!v$P$9');
define('LOGGED_IN_SALT',   'i|;2)r<h??xgW!X29@-Ok-&m|NiAzbuVOi~VNbb=#V3$$Ge3_lfNX_`T`Bw4<H2w');
define('NONCE_SALT',       '(4@&@dAe@.Wgu3</.8?%6[mZDf@tP>H};-4DG_ r0~v|-} -O%1rFxWIrR!k0.|h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tu_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');